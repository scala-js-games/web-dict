addSbtPlugin("org.scalameta"      % "sbt-scalafmt"              % "2.5.2")
addSbtPlugin("org.scala-js"       % "sbt-scalajs"               % "1.13.2")
addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject"  % "1.3.2")
addSbtPlugin("com.github.cb372"   % "sbt-explicit-dependencies" % "0.3.1")

