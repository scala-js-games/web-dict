package dict.domain

import scala.util.Try

object ListOps {

  implicit class ListSafeIndex[T](targetList: List[T]) {
    def at(index: Int): Option[T] =
      Try(targetList(index)).toOption
  }

}
