package dict

import com.raquo.laminar.api.L._
import org.scalajs.dom
import org.scalajs.dom.document
import scala.util.Random


@main
def LiveChart(): Unit =
  renderOnDomContentLoaded(
    dom.document.getElementById("app"),
    appElement()
  )



final class DataItemID

case class DataItem(id: DataItemID, label: String, price: Double, count: Int):
  def fullPrice: Double = price * count

object DataItem:
  def apply(): DataItem =
    DataItem(DataItemID(), "?", Random.nextDouble(), Random.nextInt(5) + 1)
end DataItem

type DataList = List[DataItem]

val dataVar: Var[DataList] = Var(List(DataItem(DataItemID(), "one", 1.0, 1)))
val dataSignal = dataVar.signal

def addDataItem(item: DataItem): Unit =
  dataVar.update(data => data :+ item)

def removeDataItem(id: DataItemID): Unit =
  dataVar.update(data => data.filter(_.id != id))

def appElement(): Element =
  div(
    h1("Live Chart"),
    renderDataTable(),
    renderDataList(),
  )
end appElement

def renderDataTable(): Element =
  table(
    thead(tr(th("Label"), th("Price"), th("Count"), th("Full price"), th("Action"))),
    tbody(
      children <-- dataSignal.split(_.id) { (id, initial, itemSignal) =>
        renderDataItem(id, itemSignal)
      },
    ),
    tfoot(tr(
      td(button("➕", onClick --> (_ => addDataItem(DataItem())))),
      td(),
      td(),
      td(child.text <-- dataSignal.map(data => "%.2f".format(data.map(_.fullPrice).sum))),
    )),
  )
end renderDataTable

def renderDataItem(id: DataItemID, itemSignal: Signal[DataItem]): Element =
  tr(
    td(
      inputForString(
        itemSignal.map(_.label),
        makeDataItemUpdater(id, { (item, newLabel) =>
          item.copy(label = newLabel)
        })
      )
    ),
    td(
      inputForDouble(
        itemSignal.map(_.price),
        makeDataItemUpdater(id, { (item, newPrice) =>
          item.copy(price = newPrice)
        })
      )
    ),
    td(
      inputForInt(
        itemSignal.map(_.count),
        makeDataItemUpdater(id, { (item, newCount) =>
          item.copy(count = newCount)
        })
      )
    ),
    td(
      child.text <-- itemSignal.map(item => "%.2f".format(item.fullPrice))
    ),
    td(button("🗑️", onClick --> (_ => removeDataItem(id)))),
  )
end renderDataItem

def makeDataItemUpdater[A](id: DataItemID,
                           f: (DataItem, A) => DataItem): Observer[A] =
  dataVar.updater { (data, newValue) =>
    data.map { item =>
      if item.id == id then f(item, newValue) else item
    }
  }
end makeDataItemUpdater

def inputForString(valueSignal: Signal[String],
                   valueUpdater: Observer[String]): Input =
  input(
    typ := "text",
    value <-- valueSignal,
    onInput.mapToValue --> valueUpdater,
  )
end inputForString

def inputForDouble(valueSignal: Signal[Double],
                   valueUpdater: Observer[Double]): Input =
  val strValue = Var[String]("")
  input(
    typ := "text",
    value <-- strValue.signal,
    onInput.mapToValue --> strValue,
    valueSignal --> strValue.updater[Double] { (prevStr, newValue) =>
      if prevStr.toDoubleOption.contains(newValue) then prevStr
      else newValue.toString
    },
    strValue.signal --> { valueStr =>
      valueStr.toDoubleOption.foreach(valueUpdater.onNext)
    },
  )
end inputForDouble

def inputForInt(valueSignal: Signal[Int],
                valueUpdater: Observer[Int]): Input =
  input(
    typ := "text",
    controlled(
      value <-- valueSignal.map(_.toString),
      onInput.mapToValue.map(_.toIntOption).collect {
        case Some(newCount) => newCount
      } --> valueUpdater,
    ),
  )
end inputForInt

def renderDataList(): Element =
  ul(
    children <-- dataSignal.split(_.id) { (id, initial, itemSignal) =>
      li(child.text <-- itemSignal.map(item => s"${item.count} ${item.label}"))
    }
  )
end renderDataList
