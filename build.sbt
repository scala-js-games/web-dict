


lazy val webDict = project
  .in(file("."))
  .aggregate(dict.js,dict.jvm)
  .settings(
    publish      := {},
    publishLocal := {},
  )

lazy val dict = crossProject(JSPlatform, JVMPlatform)
  .in(file("."))
  .settings(
    name                                    := "dict",
    scalaVersion                            := "3.3.1",
    libraryDependencies += "org.scalatest" %%% "scalatest" % "3.2.17" % "test",
  )
  .jsSettings(
    scalaJSUseMainModuleInitializer := true,
    libraryDependencies ++= Seq(
      "com.raquo"    %%% "laminar"       % "16.0.0",
      "io.laminext"  %%% "fetch-upickle" % "0.16.2",
      "org.scala-js" %%% "scala-js-macrotask-executor" % "1.1.1"
    ),

  )


Global / onChangedBuildSource := ReloadOnSourceChanges
